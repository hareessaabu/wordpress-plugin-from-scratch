<?php
/**
* @package AlecadddPlugin
*/
/*
Plugin Name: Alecaddd Plugin
Plugin URI: http://alecaddd.com/plugin
Description: This is my first attempt on writing a custom Plugin for this amazing tutorial series.
Version: 1.0.0
Author: Alessandro "Alecaddd" Castellani
Author URI: http://alecaddd.com
License: GPLv2 or later
Text Domain: alecaddd-plugin
*/

/* 
This program is free software; you can redistribute and modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUR ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PERTICULAR PURPOSE. See the GNU General Public License for more details.

Copyright 2005-2015 Automattic, Inc.
*/ 

defined ( 'ABSPATH' ) or die( 'Hey, what are you doing here?' );

class AlecadddPlugin
{

	public $plugin;

	//public
	//can be accessed anywhere

	//protected
	//can only be accessed in the class itself or extensions of that class

	//private
	//can be accessed only within the class itself



	function __construct(){
		$this -> plugin = plugin_basename( __FILE__ );
	}

	function register(){
		add_action('admin_enqueue_scripts', array($this, 'enqueue'));

		add_action( 'admin_menu', array( $this, 'add_admin_pages') );

		add_filter("plugin_action_links_$this->plugin", array($this, 'settings_link'));

		// add_filter($tag,$function_to_add,10,1);
	}

	public function settings_link ( $links ) {
		// add custom settings link

		$settings_link = '<a href="options-general.php?page=alecaddd_plugin"> Settings </a>';

		array_push($links, $settings_link);

		return $links;
	}

	public function add_admin_pages(){

		//add_menu_page($page_title,$menu_title,$capability,$menu_slug,'','',null);

		add_menu_page ('Alecaddd Plugin', 'Alecaddd', 'manage_options', 'alecaddd_plugin', array($this,'admin_index'),'dashicons-store', 110);
	}

	public function admin_index(){
		require_once plugin_dir_path( __FILE__ ) . 'templates/admin.php';
	}

	protected function create_post_type(){
		add_action('init',array($this, 'custom_post_type'));
	}


	function activate(){
		//generated a CPT
		//flush rewrite rules
		flush_rewrite_rules();
	}

	function deactivate(){
		//flush rewrite rules
		flush_rewrite_rules();

	}

	function uninstall(){
		//delete the CPT
		//delete all the plugin data from the DB
	}

	function custom_post_type(){
		register_post_type( 'book', ['public'=> true, 'label' => 'Books'] );
	}

	function enqueue(){
		//enqueue all our scripts
		wp_enqueue_style('mypluginstyle',plugins_url('/assets/mystyle.css', __FILE__));
		wp_enqueue_script('mypluginscript',plugins_url('/assets/myscript.js', __FILE__));
	}

	private function print_stuff(){
		echo 'Test';
	}

}


if (class_exists('AlecadddPlugin')){
	$alecadddPlugin = new AlecadddPlugin();
	$alecadddPlugin -> register();
}


//activation
require_once plugin_dir_path( __FILE__ ) . 'inc/alecaddd-plugin-activate.php';
register_activation_hook(__FILE__ , array('AlecadddPluginActivate','activate') );

//deactivation
require_once plugin_dir_path( __FILE__ ) . 'inc/alecaddd-plugin-activate.php';
register_deactivation_hook(__FILE__ , array('AlecadddPluginActivate','deactivate') );




